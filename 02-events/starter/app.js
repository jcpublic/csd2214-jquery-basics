// jquery setup
// $(document).ready(function() {
//
// });


// 1. SAY HELLO BUTTON - When person clicks on button, show an alert() that says "Hello, how are you?"
document.querySelector("#btn-hello").addEventListener("click", function() {
  alert("Hello, how are you?");
})


// 2. CHANGE BLUE BUTTON - When person clicks on button, change webpage background to blue.

// 3. TURN ON LIGHTS -  Pressing this button causes the background to alternate between black and white. (Hint: Use toggleClass)
// The first time the person presses the button, change background to black
// The second time person presses button, change background to white
// The third time person presses button, change background to black
// The fourth time person presses button, change background to white
// etc, etc, etc.

// 4. Update the code for the TURN ON LIGHTS button:
// - Extend your code from #3 so that the text of the button also changes.
// - Text of button should alternate between "Turn Lights On" and "Turn Lights Off"

// 5. TYPE SOMETHING BOX:  When person types something in the box, output a message to the screen that says "YOU TYPED ____", where ___ is the letter the person pressed on the keyboard.

// 6. DETECT WHICH BOX WAS CLICKED:  On the html page, there are 3 pokemon (pikachu, pokeball, caterpie). Using JQuery, write the code to detect which pokemon the person clicked.
const boxClicked = function(event) {
  // get id of the box that was clicked
  let box = event.target;
  console.log(box);

  // get the <p> element in the box
  let p = box.querySelector("p");
  console.log(p.innerText);
}
document.querySelector(".boxes").addEventListener("click", boxClicked);