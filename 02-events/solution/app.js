// jquery setup
$(document).ready(function() {
  console.log("HELLO");

  // 1. SAY HELLO BUTTON - When person clicks on button, show an alert() that says "Hello, how are you?"
  // document.querySelector("#btn-hello").addEventListener("click", function() {
  //   alert("Hello, how are you?");
  // })

  $("#btn-hello").click(function(){
    alert("Hello how are you?");
  })

  // 2. CHANGE BLUE BUTTON - When person clicks on button, change webpage background to blue.
  $("#btn-blue").click(function() {
    $("body").css("background-color","blue");
  })

  // 3. TURN ON LIGHTS -  Pressing this button causes the background to alternate between black and white. (Hint: Use toggleClass)
  // The first time the person presses the button, change background to black
  // The second time person presses button, change background to white
  // The third time person presses button, change background to black
  // The fourth time person presses button, change background to white
  // etc, etc, etc.
  $("#btn-lights").click(function(){
    // toggleClass is alternating between:
    // addClass --> removeClass --> addClass --> removeClass
    $("body").toggleClass("dark");


    // 4. Update the code for the TURN ON LIGHTS button:
    // - Extend your code from #3 so that the text of the button also changes.
    // - Text of button should alternate between "Turn Lights On" and "Turn Lights Off"
    
    // Example of changing the text when you have a <span> around the "ON" word
    // let buttonText = $("span#light-status").text();
    // if (buttonText === "ON") {
    //   $("span#light-status").text("OFF");
    // }
    // else {
    //   $("span#light-status").text("ON");
    // }


    // Example of using the "this" keyword
    // console.log(this);
    // console.log($(this).text());
    // console.log(this.innerText);


    let buttonText = $(this).text();
    if (buttonText === "TURN ON LIGHTS") {
      $(this).text("TURN OFF LIGHTS");
    }
    else {
      $(this).text("TURN ON LIGHTS");
    }

  });


  // 6. DETECT WHICH BOX WAS CLICKED:  On the html page, there are 3 pokemon (pikachu, pokeball, caterpie). Using JQuery, write the code to detect which pokemon the person clicked.
  // $(".boxes").click(function(event){
    
  //   let box = event.target; // div.box
  //   console.log(box);

  //   let test = $(box).find("p");
  //   console.log($(test).text())
  // });

  $("div.box").click(function(){
    console.log(this);
    let pokemon = $(this).find("p").text();
    console.log(pokemon);
  })


  // 5. TYPE SOMETHING BOX:  When person types something in the box, output a message to the screen that says "YOU TYPED ____", where ___ is the letter the person pressed on the keyboard.
  $("#input-box").keypress(function(event){
    // what code do you want to execute when the person presses a butnoon on the keyboard?
    console.log("You typed something in the box!");

    // 1. what key was actually pressed
    console.log(event.which);

    // 2 . output the letter to the screen
    let key = String.fromCharCode(event.which);
    console.log(key);

    $("p.results-label").text("You pressed: " + key + " button");

  })
});

// 6. DETECT WHICH BOX WAS CLICKED:  On the html page, there are 3 pokemon (pikachu, pokeball, caterpie). Using JQuery, write the code to detect which pokemon the person clicked.
// const boxClicked = function(event) {
//   // get id of the box that was clicked
//   let box = event.target;
//   console.log(box);

//   // get the <p> element in the box
//   let p = box.querySelector("p");
//   console.log(p.innerText);
// }
// document.querySelector(".boxes").addEventListener("click", boxClicked);