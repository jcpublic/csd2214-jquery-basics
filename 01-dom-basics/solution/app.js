// EXAMPLES WITH JQUERY
$(document).ready(function() {
    console.log("HELLO WORLD!");

    // 1. Change the text inside the <h1> to say: I AM A DINOSAUR
    $("h1").text("I AM A DINOSAUR");

    // 2. Select the <P> using its class attribute. Print out the text inside the <P> to the console.
    console.log($(".greeting").text());         // .text() --> getter
    
    console.log(document.querySelector(".greeting").innerText);

    // 3. Change the text "Javascript is fun!" to "Jquery is fun!".  To do this, use an id selector on the <span> element to change the word Javascript to Jquery.
    $("#lang").text("Jquery");              // .text() --> setter function

    
    // 4. Change the background color of all <li> tags to YELLOW background
    // let lis = document.querySelectorAll("li");
    // for (let i = 0; i < lis.length; i++) {      
    //     let li = lis[i];
    //     li.style.backgroundColor="yellow";
    // }
    $("li").css("background-color", "yellow");


    // 7. Using a single JQuery statement, update the font size and color of all <h2> elements as follows:
    // - change font size to be 30px;
    // - change color to blue;
    // $("h2").css("font-size", "30px");
    // $("h2").css("color", "blue");
    // $("h2").css(
    //     {   "font-size":"30px", 
    //         "color":"blue",
    //         "background-color":"black"
    //     });

    // 8. Use Jquery to add the "highlight" class to the "Pictures and Links" element

    // using the eq() function
    //$("h2").eq(1).addClass("highlight");
    //$("h2:eq(1)").addClass("highlight");

    // using the next() function
    $("ul").next().addClass("highlight");
    $("ul").next().removeClass("line");

    // 9. Use Jquery to add the "line" class to the <figure> element
    $("figure").addClass("line");

    // 5. Change the image from nyancat to the donut
    $("img").attr("src", "img/donut.png");
    
    // 6. Update the <a> link to open www.snapchat.com (instead of facebook)
    $("a").attr("href", "http://www.snapchat.com");

});





