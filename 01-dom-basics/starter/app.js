// EXAMPLES WITH JQUERY

// 1. Change the text inside the <h1> to say: I AM A DINOSAUR

// 2. Select the <P> using its class attribute. Print out the text inside the <P> to the console.

// 3. Change the text "Javascript is fun!" to "Jquery is fun!".  To do this, use an id selector on the <span> element to change the word Javascript to Jquery.

// 4. Change the background color of all <li> tags to YELLOW

// 5. Change the image from nyancat to the donut

// 6. Update the <a> link to open www.snapchat.com (instead of facebook)

// 7. Using a single JQuery statement, update the font size and color of all <h2> elements as follows:
// - change font size to be 30px;
// - change color to blue;


// 8. Use Jquery to add the "highlight" class to the "Pictures and Links" element

// 9. Use Jquery to add the "line" class to the <figure> element

